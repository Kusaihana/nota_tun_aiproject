function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local myTeamID = Spring.GetMyTeamID()
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)
	-- find bear
	local bearId = UnitDefNames["armthovr"].id
	local bear = Spring.GetTeamUnitsByDefs(myTeamID,bearId)
	local x,y,z = Spring.GetUnitPosition(bear[1])

	--local unloadID = CMD.UNLOAD
	--local cmdid = Spring.FindUnitCmdDesc(bear[1], unloadID)

	-- unload units to the bear
	local loadDesc
	local cmdDescs = Spring.GetUnitCmdDescs(bear[1])
	for i=1,#cmdDescs do
		if cmdDescs[i].action == "unloadunits" then
			loadDesc = cmdDescs[i]
		end
	end

	local unloadPos = Vec3(x+100,y,z+100)

	SpringGiveOrderToUnit(bear[1], loadDesc.id, unloadPos:AsSpringVector(), {})
	local nearestEnemy = Spring.GetUnitNearestEnemy(bear[1])

	if (Spring.GetUnitIsTransporting(bear[1]) == nil or nearestEnemy == nil) then
		return SUCCESS
	else
		return RUNNING
	end
end