function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT,
		parameterDefs = {
			{ 
				name = "atlasId",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "unitToCarryId",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speedups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local loadID = CMD.LOAD_UNITS
local moveID = CMD.MOVE

function Run(self, units, parameter)
	local atlasId = parameter.atlasId
	local unitToCarryId = parameter.unitToCarryId

	if not Spring.ValidUnitID(atlasId) then
		return FAILURE
	else

		SpringGiveOrderToUnit(atlasId, loadID, {unitToCarryId}, {})


		if (Spring.GetUnitTransporter(unitToCarryId) == nil) then
			return RUNNING
		else
			return SUCCESS
		end
	end

end