function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT, 
		parameterDefs = {
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speedups
local myTeamID = Spring.GetMyTeamID()
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)
	local position = parameter.position

	-- find bear
	local bearId = UnitDefNames["armthovr"].id
	local bear = Spring.GetTeamUnitsByDefs(myTeamID,bearId)

	-- move the bear
	local moveID = CMD.MOVE
	local x,y,z = Spring.GetUnitPosition(bear[1])
	local pointmanPosition = Vec3(x,y,z)

	if (pointmanPosition:Distance(position) < 500) then
		return SUCCESS
	else
		SpringGiveOrderToUnit(bear[1], moveID, position:AsSpringVector(), {})
		return RUNNING
	end
end