function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local myTeamID = Spring.GetMyTeamID()
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local transfered = false

function Run(self, units, parameter)
	-- find bear
	local bearId = UnitDefNames["armthovr"].id
	local bear = Spring.GetTeamUnitsByDefs(myTeamID,bearId)
	local x,y,z = Spring.GetUnitPosition(bear[1])

	--local loadID = CMD.LOAD

	-- load units to the bear
	local lastUnitIndex = 1 

	if #units > 0 then
		for i=1,#units do
			if units[i] ~= bear[1] then
				-- find command id for load and unload
				local loadDesc
				local cmdDescs = Spring.GetUnitCmdDescs(units[i])
				for i=1,#cmdDescs do
					if cmdDescs[i].action == "loadonto" then
						loadDesc = cmdDescs[i]
					end
				end
				SpringGiveOrderToUnit(units[i], loadDesc.id, {bear[1]}, {})
				lastUnitIndex = i
			end
		end
	end

	if transfered then
		return SUCCESS
	end

	if (Spring.GetUnitTransporter(units[lastUnitIndex]) == nil) then
		return RUNNING
	else
		transfered = true
		return SUCCESS
	end
end