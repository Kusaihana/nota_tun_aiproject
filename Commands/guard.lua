function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT,
		parameterDefs = {
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speedups
local myTeamID = Spring.GetMyTeamID()
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local atlasId = UnitDefNames["armatlas"].id
local peeperId = UnitDefNames["armpeep"].id

local guardID = CMD.GUARD

function Run(self, units, parameter)
	local position = parameter.position

	-- find atlases
	local atlases = Spring.GetTeamUnitsByDefs(myTeamID,atlasId)
	local mypeepers = Spring.GetTeamUnitsByDefs(myTeamID,peeperId)

	if #mypeepers > 0 then
		if Spring.GetUnitLastAttacker(atlases[1]) == nil then
			return RUNNING
		else
			for i=1,#mypeepers do
				SpringGiveOrderToUnit(mypeepers[i], guardID, {atlases[1]}, {})
			end
		end
	end

	if #mypeepers > 0 then
		return RUNNING
	else
		return FAILURE
	end

end