function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT,
		parameterDefs = {
			{ 
				name = "zeusID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

local attackID = CMD.FIGHT
-- speedups
local myTeamID = Spring.GetMyTeamID()
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)

	local zeusID = parameter.zeusID
	local x,y,z = Spring.GetUnitPosition(zeusID)

	local nearestEnemy = Spring.GetUnitNearestEnemy(zeusID)

	SpringGiveOrderToUnit(zeusID, attackID, {}, {})

	if nearestEnemy then
		return RUNNING
	else
		return SUCCESS
	end
end