function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT,
		parameterDefs = {
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "farckId",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speedups
local myTeamID = Spring.GetMyTeamID()
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local moveID = CMD.MOVE

function Run(self, units, parameter)
	local position = parameter.position
	local farckId = parameter.farckId

	if farckId == nil then
		return FAILURE
	else
		if Spring.GetUnitIsDead(farckId) then
			return FAILURE
		else
			local x,y,z = Spring.GetUnitPosition(farckId)
			local atlasPos = Vec3(x,y,z)
			local aimPos = Vec3(position.x,y,position.z)

			local movePos = Vec3(x,0,position.z)
			if (math.abs(z - position.z) > 200) then
				SpringGiveOrderToUnit(farckId, moveID, movePos:AsSpringVector(), {})
			else
				SpringGiveOrderToUnit(farckId, moveID, position:AsSpringVector(), {})
			end

			if (atlasPos:Distance(aimPos) > 400) then
				
				return RUNNING
			else
				return SUCCESS
			end
		end
		
	end

end