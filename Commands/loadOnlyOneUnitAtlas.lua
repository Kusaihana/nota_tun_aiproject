function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT,
		parameterDefs = {
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "atlasId",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "unitToCarryId",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speedups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local loadID = CMD.LOAD_UNITS
local moveID = CMD.MOVE

function Run(self, units, parameter)
	local position = parameter.position
	local atlasId = parameter.atlasId
	local unitToCarryId = parameter.unitToCarryId

	if Spring.GetUnitIsDead(atlasId) or Spring.GetUnitIsDead(unitToCarryId) then
		return FAILURE
	else
		local x,y,z = Spring.GetUnitPosition(unitToCarryId)
		local curx,cury,curz = Spring.GetUnitPosition(atlasId)
		local movePos = Vec3(x,0,curz)

		if (math.abs(curx - x) > 300) then
			SpringGiveOrderToUnit(atlasId, moveID, movePos:AsSpringVector(), {})
		else
			SpringGiveOrderToUnit(atlasId, loadID, {unitToCarryId}, {})
		end

		if (Spring.GetUnitTransporter(unitToCarryId) == nil) then
			return RUNNING
		else
			return SUCCESS
		end
	end

end