function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT,
		parameterDefs = {
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speedups
local myTeamID = Spring.GetMyTeamID()
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local atlasId = UnitDefNames["armatlas"].id
local boxId = UnitDefNames["armbox"].id

local loadID = CMD.LOAD_UNITS
local moveID = CMD.MOVE
local unloadID = CMD.UNLOAD_UNITS

function Run(self, units, parameter)
	local position = parameter.position

	-- find atlases
	local atlases = Spring.GetTeamUnitsByDefs(myTeamID,atlasId)
	local x,y,z = Spring.GetUnitPosition(atlases[1])

	-- find units to load
	local myUnits = Spring.GetTeamUnitsByDefs(myTeamID,boxId)

	local successCount = 0

	for i=1,#atlases do
		local x,y,z = Spring.GetUnitPosition(atlases[i])
		local atlasPos = Vec3(x,y,z)
		local aimPos = Vec3(position.x+i*20,y,position.z+i*25)
		if (atlasPos:Distance(aimPos) > 500) then
			SpringGiveOrderToUnit(atlases[i], moveID, aimPos:AsSpringVector(), {})
		else
			successCount = successCount + 1
		end
	end

	if successCount == #atlases then
		return SUCCESS
	else
		return RUNNING
	end
end