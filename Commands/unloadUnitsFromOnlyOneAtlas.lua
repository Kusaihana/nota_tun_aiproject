function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT,
		parameterDefs = {
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "atlasId",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speedups
local myTeamID = Spring.GetMyTeamID()
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local moveID = CMD.MOVE
local unloadID = CMD.UNLOAD_UNIT

function Run(self, units, parameter)
	local position = parameter.position
	local atlasId = parameter.atlasId

	if not Spring.ValidUnitID(atlasId) then
		return FAILURE
	end

	local x,y,z = Spring.GetUnitPosition(atlasId)
	local atlasPos = Vec3(x,y,z)			

	SpringGiveOrderToUnit(atlasId, unloadID, {atlasPos.x, atlasPos.y, atlasPos.z}, {})
	SpringGiveOrderToUnit(atlasId, moveID, position:AsSpringVector(), {"shift"})
	
	if (atlasPos:Distance(position) > 200) then
		return RUNNING
	else
		return SUCCESS
	end

end