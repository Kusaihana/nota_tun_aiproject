function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT,
		parameterDefs = {
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speedups
local myTeamID = Spring.GetMyTeamID()
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local atlasId = UnitDefNames["armatlas"].id
local boxId = UnitDefNames["armbox"].id
local hammerId = UnitDefNames["armham"].id
local lltId = UnitDefNames["armmllt"].id
local peeperId = UnitDefNames["armpeep"].id

local loadID = CMD.LOAD_UNITS
local moveID = CMD.MOVE
local unloadID = CMD.UNLOAD_UNITS

function Run(self, units, parameter)
	local position = parameter.position

	-- find atlases
	local atlases = Spring.GetTeamUnitsByDefs(myTeamID,atlasId)
	-- find units to load
	local myUnits = Spring.GetTeamUnitsByDefs(myTeamID,boxId)
	local myGroundUnits = Spring.GetTeamUnitsByDefs(myTeamID,hammerId)
	local myllts = Spring.GetTeamUnitsByDefs(myTeamID,lltId)
	local mypeepers = Spring.GetTeamUnitsByDefs(myTeamID,peeperId)

	table.sort(myUnits, compare)
	table.sort(myllts, compare)

	-- remove the far units
	local unitsToCarry = {}
	if #myUnits > 0 then
		for i=1,#myUnits do
			local x,y,z = Spring.GetUnitPosition(myUnits[i])
			local tempPos = Vec3(x,y,z)
	        if (tempPos:Distance(position) < 5000) then
				unitsToCarry[#unitsToCarry+1] = myUnits[i]
			end
	    end
	end

	if #myllts > 0 then
		for i=1,#myllts do
			local x,y,z = Spring.GetUnitPosition(myllts[i])
			local tempPos = Vec3(x,y,z)
	        if (tempPos:Distance(position) < 5000) then
				unitsToCarry[#unitsToCarry+1] = myllts[i]
			end
	    end
	end

	if #myGroundUnits > 0 then
		for i=1,#myGroundUnits do
	        unitsToCarry[#unitsToCarry+1] = myGroundUnits[i]
	    end
	end


	-- sort units by distance
	function compare(a,b)
		local xa,ya,za = Spring.GetUnitPosition(a)
		local apos = Vec3(xa,ya,za)
		local xb,yb,zb = Spring.GetUnitPosition(b)
		local bpos = Vec3(xb,yb,zb)
  		return position:Distance(apos) < position:Distance(bpos)
	end
	

	if #atlases > 0 then
		for i=1,#atlases do
			-- if there is an enemy on the path try to avoid
			-- or go with peepers
			SpringGiveOrderToUnit(atlases[i], loadID, {unitsToCarry[i]}, {})
		end
	end

	local successCount = 0

	for i=1,#unitsToCarry do
		if (Spring.GetUnitTransporter(unitsToCarry[i]) == nil) then
			successCount = successCount
		else
			successCount = successCount + 1
		end
	end

	if successCount > 16 then
		return SUCCESS
	else
		return RUNNING
	end
end