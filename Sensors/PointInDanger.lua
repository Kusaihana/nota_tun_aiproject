local sensorInfo = {
	name = "PointInDanger",
	desc = "Return.",
	author = "Merve Tuncel",
	date = "2018-06-22",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local myTeamID = Spring.GetMyTeamID()

-- @description return available farcks
return function(pos)
	local missionInfo = Sensors.core.MissionInfo()

	local mid = missionInfo.corridors.Middle.points

	local danger = pos 

	for i=1,#mid do
		--if mid[i].isStrongPoint then
			units = Spring.GetUnitsInSphere(mid[i].position.x, mid[i].position.y, mid[i].position.z, 500)

			for j=1,#units do
				unitTeam = Spring.GetUnitTeam(units[j])
				if not Spring.AreTeamsAllied(myTeamID,unitTeam) then
					--danger = mid[i].position
					local pointX, pointY, pointZ = Spring.GetUnitPosition(units[j])
					local pointmanPosition = Vec3(pointX, pointY, pointZ)
					return pointmanPosition
				end
			end
		--end
	end


	return danger
end