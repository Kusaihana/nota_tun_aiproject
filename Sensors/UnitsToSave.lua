local sensorInfo = {
	name = "UnitsToSave",
	desc = "Return unit list.",
	author = "Merve Tuncel",
	date = "2018-06-05",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local myTeamID = Spring.GetMyTeamID()

local atlasId = UnitDefNames["armatlas"].id
local boxId = UnitDefNames["armbox"].id
local hammerId = UnitDefNames["armham"].id
local rockerId = UnitDefNames["armrock"].id
local lltId = UnitDefNames["armmllt"].id
local peeperId = UnitDefNames["armpeep"].id

-- @description return available units to carry
return function(position)
	-- find units to load
	local myUnits = Spring.GetTeamUnitsByDefs(myTeamID,boxId)
	local myGroundUnits = Spring.GetTeamUnitsByDefs(myTeamID,hammerId)
	local myGroundUnits2 = Spring.GetTeamUnitsByDefs(myTeamID,rockerId)
	local myllts = Spring.GetTeamUnitsByDefs(myTeamID,lltId)
	local mypeepers = Spring.GetTeamUnitsByDefs(myTeamID,peeperId)

	table.sort(myUnits, compare)
	table.sort(myllts, compare)

	-- remove the far units
	local unitsToCarry = {}
	if #myUnits > 0 then
		for i=1,#myUnits do
			local x,y,z = Spring.GetUnitPosition(myUnits[i])
			local tempPos = Vec3(x,y,z)
	        if (tempPos:Distance(position) < 6000) then
	        	-- also remove the units that already in the safe area
	        	if (tempPos:Distance(position) > 600) then
					unitsToCarry[#unitsToCarry+1] = myUnits[i]
				end
			end
	    end
	end

	if #myllts > 0 then
		for i=1,#myllts do
			local x,y,z = Spring.GetUnitPosition(myllts[i])
			local tempPos = Vec3(x,y,z)
	        if (tempPos:Distance(position) < 6000) then
	        	-- also remove the units that already in the safe area
	        	if (tempPos:Distance(position) > 1000) then
	        		-- alsooo check nearby danger
	        		local nearestEnemy = Spring.GetUnitNearestEnemy(myllts[i],500)
	        		if nearestEnemy == nil then
						unitsToCarry[#unitsToCarry+1] = myllts[i]
					end
				end
			end
	    end
	end

	if #myGroundUnits > 0 then
		for i=1,#myGroundUnits do
			local x,y,z = Spring.GetUnitPosition(myGroundUnits[i])
			local tempPos = Vec3(x,y,z)
			if (tempPos:Distance(position) < 6000) then
				if (tempPos:Distance(position) > 1000) then
	        		unitsToCarry[#unitsToCarry+1] = myGroundUnits[i]
	    		end
	    	end
	    end
	end

	if #myGroundUnits2 > 0 then
		for i=1,#myGroundUnits2 do
			local x,y,z = Spring.GetUnitPosition(myGroundUnits2[i])
			local tempPos = Vec3(x,y,z)
			if (tempPos:Distance(position) < 6000) then
				if (tempPos:Distance(position) > 600) then
	        		unitsToCarry[#unitsToCarry+1] = myGroundUnits2[i]
	    		end
	    	end
	    end
	end

	-- sort units by distance
	function compare(a,b)
		local xa,ya,za = Spring.GetUnitPosition(a)
		local apos = Vec3(xa,ya,za)
		local xb,yb,zb = Spring.GetUnitPosition(b)
		local bpos = Vec3(xb,yb,zb)
  		return position:Distance(apos) < position:Distance(bpos)
	end

	return unitsToCarry
end