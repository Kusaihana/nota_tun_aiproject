local sensorInfo = {
	name = "AvailableZeuses",
	desc = "Return zeus list.",
	author = "Merve Tuncel",
	date = "2018-06-22",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local farckId = UnitDefNames["armzeus"].id
local myTeamID = Spring.GetMyTeamID()

-- @description return available farcks
return function()
	local farkcs = Spring.GetTeamUnitsByDefs(myTeamID,farckId)

	local available = {}

	if #farkcs > 0 then
		for i=1,#farkcs do
	        --if (table.getn(Spring.GetUnitIsTransporting(farkcs[i])) == 0) then
				available[#available+1] = farkcs[i]
			--end
	    end
	end

	return available
end