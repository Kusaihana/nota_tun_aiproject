local sensorInfo = {
	name = "Map",
	desc = "Return the hill positions.",
	author = "Merve Tuncel",
	date = "2018-05-13",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local myTeamID = Spring.GetMyTeamID()

-- @description return hill positions
return function()
	local mapx = Game.mapSizeX
	local mapz = Game.mapSizeZ

	local listOfMaxHeights = {0,0,0,0}

	local listOfHills = {}
	
	for i=1, 4 do
		listOfHills[i] = {}
		for j=1,2 do
			listOfHills[i][j] = 0
		end
	end

	local flag = true;

	for i=0, mapx, 150 do --4096
		for j=0, mapz, 100 do --1024
			local h = Spring.GetGroundHeight(i,j)
			for l=1, 4 do
				if (h > listOfMaxHeights[l] and flag) then
					listOfMaxHeights[l] = h
					listOfHills[l][1] = i
					listOfHills[l][2] = j
					flag = false
				end
			end
			flag = true
		end
	end


	return listOfHills
end