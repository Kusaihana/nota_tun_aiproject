local sensorInfo = {
	name = "Commander",
	desc = "Return data of commander position.",
	author = "Merve Tuncel",
	date = "2018-05-10",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local myTeamID = Spring.GetMyTeamID()

-- @description return commander position
return function()
	local commanderDef = UnitDefNames["armbcom"].id
	local commander = Spring.GetTeamUnitsByDefs(myTeamID,commanderDef)
	local x,y,z = Spring.GetUnitPosition(commander[1])
	return Vec3(x,y,z)
end