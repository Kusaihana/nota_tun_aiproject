local sensorInfo = {
	name = "UnitsToTransport",
	desc = "Return unit list.",
	author = "Merve Tuncel",
	date = "2018-06-05",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local myTeamID = Spring.GetMyTeamID()

local seerId = UnitDefNames["armseer"].id
local boxId = UnitDefNames["armbox"].id
local artId = UnitDefNames["armmart"].id

-- @description return available units to carry
return function(position)
	-- find units to load
	local myUnits = Spring.GetTeamUnitsByDefs(myTeamID,seerId)
	local myUnits3 = Spring.GetTeamUnitsByDefs(myTeamID,boxId)
	local myUnits2 = Spring.GetTeamUnitsByDefs(myTeamID,artId)

	--table.sort(myUnits, compare)
	table.sort(myUnits2, compare)
	table.sort(myUnits3, compare)

	-- remove the far units
	local unitsToCarry = {}
	if #myUnits > 0 then
		for i=1,#myUnits do
			local x,y,z = Spring.GetUnitPosition(myUnits[i])
			local tempPos = Vec3(x,y,z)

        	if (Spring.GetUnitTransporter(myUnits[i]) == nil) then
        		if (tempPos:Distance(position) > 2500) then
					unitsToCarry[#unitsToCarry+1] = myUnits[i]
				end
			end

	    end
	end

	if #myUnits2 > 0 then
		for i=1,#myUnits2 do
			local x,y,z = Spring.GetUnitPosition(myUnits2[i])
			local tempPos = Vec3(x,y,z)

			if (Spring.GetUnitTransporter(myUnits2[i]) == nil) then
				if (tempPos:Distance(position) > 2000) then
					unitsToCarry[#unitsToCarry+1] = myUnits2[i]
				end

			end

	    end
	end

	if #myUnits3 > 0 then
		for i=1,#myUnits3 do
			local x,y,z = Spring.GetUnitPosition(myUnits3[i])
			local tempPos = Vec3(x,y,z)

			if (Spring.GetUnitTransporter(myUnits3[i]) == nil) then
				if (tempPos:Distance(position) > 1500) then
					unitsToCarry[#unitsToCarry+1] = myUnits3[i]
				end

			end

	    end
	end

	-- sort units by distance
	function compare(a,b)
		local xa,ya,za = Spring.GetUnitPosition(a)
		local apos = Vec3(xa,ya,za)
		local xb,yb,zb = Spring.GetUnitPosition(b)
		local bpos = Vec3(xb,yb,zb)
  		return position:Distance(apos) > position:Distance(bpos)
	end

	return unitsToCarry
end