local sensorInfo = {
	name = "Group",
	desc = "Return unit list.",
	author = "Merve Tuncel",
	date = "2018-05-13",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return selected units
return function()
	local unitList = Spring.GetSelectedUnits()

	return unitList
end