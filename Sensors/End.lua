local sensorInfo = {
	name = "End",
	desc = "End mission.",
	author = "Merve Tuncel",
	date = "2018-05-13",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0

	-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

function getInfo()
    return {
        period = 0 -- no caching
    }
end

-- @description return current wind statistics
return function()
    message.SendRules({
        subject = "CTP_playerTriggeredGameEnd",
        data = {},
    })
end
