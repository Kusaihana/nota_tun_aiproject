local sensorInfo = {
	name = "AvailableAtlases",
	desc = "Return atlas list.",
	author = "Merve Tuncel",
	date = "2018-06-05",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local atlasId = UnitDefNames["armatlas"].id
local myTeamID = Spring.GetMyTeamID()

-- @description return available atlases
return function()
	local atlases = Spring.GetTeamUnitsByDefs(myTeamID,atlasId)

	local available = {}

	if #atlases > 0 then
		for i=1,#atlases do
	        if (table.getn(Spring.GetUnitIsTransporting(atlases[i])) == 0) then
				available[#available+1] = atlases[i]
			end
	    end
	end

	return available
end